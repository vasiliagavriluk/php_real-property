<?php include(PathList::GetPath(PathList::FILE_PATH_VIEW).'header.php'); ?>

    <div id="app">

        <div class="site-mobile-menu site-navbar-target">
            <div class="site-mobile-menu-header">
                <div class="site-mobile-menu-close">
                    <span class="icofont-close js-menu-toggle"></span>
                </div>
            </div>
            <div class="site-mobile-menu-body"></div>
        </div>

        <nav class="site-nav">
            <div class="container">
                <div class="menu-bg-wrap">
                    <div class="site-navigation">
                        <a href="/" class="logo m-0 float-start">Сайт недвижимости</a>
                        <ul class="js-clone-nav d-none d-lg-inline-block text-start site-menu float-end">
                            <li class="active"><a href="/">Главная</a></li>
                            <li><a href="/admin">Админ-панель</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="section section-properties" style="padding-top: 150px">
            <div class="container">
                <div class="row">
                    <div class="sorting_bl">
                        <span>Сортировать:</span>
                        <a class="sorting_el min" href="#"><i></i>По цене</a>

                        <div class="search_right">
                            <input type="text" class="search_input"  placeholder="Поиск по номеру дома">
                            <input type="submit" class="search_btn" value="найти">
                        </div>

                    </div>
                    <div id="item"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function ()
        {
            load();
            function load()
            {
                $.ajax({
                    type: 'POST',
                    url: 'home/all',
                    success:function(data)
                    {
                        $.each((data),function(key,item)
                        {
                            _setHTML(item);
                        });
                    }
                });
            };

            $(".sorting_el").on("click", function(e)
            {
                e.preventDefault();
                if($(this).hasClass('min')){
                    $('.sorting_el').removeClass('min');
                    $('.sorting_el').toggleClass('max');
                    _postSorting("DESC");
                }
                else
                {
                    $('.sorting_el').removeClass('max');
                    $('.sorting_el').toggleClass('min');
                    _postSorting("ASC");
                }
            });

            function _postSorting(order)
            {
                $('#item').empty();
                $.ajax({
                    type: 'POST',
                    url: 'home/sorting',
                    data: {
                        order: order
                    },
                    success:function(data)
                    {
                        $.each((data),function(key,item)
                        {
                            _setHTML(item);
                        });
                    }
                });

            }

            $(".search_btn").on("click", function(e)
            {
                e.preventDefault();
                var search_input = $('.search_input').val();

                $('#item').empty();
                $.ajax({
                    type: 'POST',
                    url: 'home/search',
                    data: {
                        search: search_input
                    },
                    success:function(data)
                    {
                        $.each((data),function(key,item)
                        {
                            _setHTML(item);
                        });
                    }
                });
            });

            function _setHTML(item)
            {
                var street = item["street"]+' '+item["house"]+'-'+item["apartment"];

                $('#item').append(
                    '<div class="col-item col-xs-12 col-sm-6 col-md-6 col-lg-4">'+
                    '<div class="property-item mb-30">'+
                    '<div  class="img">'+
                    '   <img src="home/image/'+item["id_image"]+'" alt="Image" class="img-fluid">'+
                    '</div>'+
                    '<div class="property-content">'+
                    '   <div class="price mb-2"><span>'+item["price"]+' &#8381;</span></div>'+
                    '<div>'+
                    '<span class="d-block mb-2 text-black-50">'+street+'</span>'+
                    '<span class="city d-block mb-3">'+item["object_name"]+'</span>'+
                    '<div class="specs d-flex mb-4">'+
                    '<span class="d-block d-flex align-items-center me-3">'+
                    '<span class="icon-info me-2"></span>'+
                    '<span class="caption">'+item["description"]+'</span>'+
                    '</span>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>');
            }
        });
    </script>

<?php include(PathList::GetPath(PathList::FILE_PATH_VIEW).'footer.php'); ?>