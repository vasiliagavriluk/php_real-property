<?php include(PathList::GetPath(PathList::FILE_PATH_VIEW).'header.php'); ?>


<div id="app">

    <!-- START: header -->

    <div class="probootstrap-loader"></div>

    <nav class="site-nav">
        <div class="container">
            <div class="menu-bg-wrap">
                <div class="site-navigation">
                    <a href="/" class="logo m-0 float-start">Админ панель</a>
                    <ul class="js-clone-nav d-none d-lg-inline-block text-start site-menu float-end">
                        <li class="active"><p><?php echo($_SESSION['user']); ?></p></li>
                        <li><a href="/admin/close">Выйти</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div class="container_add" style="padding: 150px 0">
    <section class="probootstrap-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <button type="button" id="modal_btn" class="btn btn-light" data-toggle="modal" data-target="#exampleModal">
                        Добавить
                    </button>
                    <table class="table">
                        <tr>
                            <th>Наименование объекта</th>
                            <th>Адрес</th>
                            <th>Цена</th>
                            <th>Описание</th>
                            <th>Статус</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
    </div>

    <!-- Modal -->
    <div class="modal" id="exampleModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                    <!--Наименование объекта-->
                    <div class="col-12 mb-3">
                        <input type="text" id="object_name" class="form-control" placeholder="Наименование объекта">
                    </div>

                    <div class="col-6 mb-3">
                        <input type="text" id="street" class="form-control" placeholder="Улица">
                    </div>
                    <div class="col-3 mb-3">
                        <input type="text" id="house" class="form-control" placeholder="Дом">
                    </div>
                    <div class="col-3 mb-3">
                        <input type="text" id="apartment" class="form-control" placeholder="Кв">
                    </div>

                    <div class="col-12 mb-3">
                        <input type="text" id="price" class="form-control" placeholder="Цена  &#8381;">
                    </div>
                    <div class="col-12 mb-3">
                        <textarea id="description" cols="30" rows="3" class="form-control" placeholder="Описание"></textarea>
                    </div>

                    <div class="col-12 mb-3">
                        <input id="checkbox" type="checkbox" checked>
                        <label>Актуальна</label>
                    </div>




                    <div class="col-12 mb-3">
                        <input type="file" accept="image/*" id="image-upload" />
                        <figure>
                            <img id="image" />
                        </figure>
                    </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-save" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Подложка под модальным окном -->
    <div class="overlay" id="overlay-modal"></div>

</div>
    <script>
        $(function ()
        {
            var typeModal;
            var UpdateID;
            load();

            function load()
            {
                $.ajax({
                    type: 'POST',
                    url: 'admin/all_list',
                    success:function(data)
                    {
                        $.each(JSON.parse(data),function(key,item){
                            $('.table tbody').append(item);
                        });
                    }
                });
            };

            $('body').on('click', '.testimonial-nav', function (e)
            {
                var id = $(this).data("id");
                UpdateID = id;
                e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: 'admin/edit',
                    data: {
                        id: id
                    },
                    success:function(arrData)
                    {
                        $('#object_name').val(arrData["object_name"]);
                        $('#street').val(arrData["street"]);
                        $('#house').val(arrData["house"]);
                        $('#apartment').val(arrData["apartment"]);
                        $('#price').val(arrData["price"]);
                        $('#description').val(arrData["description"]);

                        if (arrData["status"] == 1){ $('#checkbox').prop('checked', true);}
                        else { $('#checkbox').prop('checked', false); }

                        $("#image").attr("src", "admin/image/"+arrData["id_image"]);

                        $('#exampleModalLabel').html("Изменить");
                        $('#exampleModal').css('display','block');
                        overlay.classList.add('active');
                        typeModal = "update";
                    }
                });
            });

            var overlay = document.querySelector('#overlay-modal');

            function valClear()
            {
                $('#object_name').val('');
                $('#street').val('');
                $('#house').val('');
                $('#apartment').val('');
                $('#price').val('');
                $('#description').val('');
                $('#image-upload').val("");
                $('#image').attr("src","");
            }


            $("#modal_btn").on("click", function(e)
            {
                e.preventDefault();
                valClear();
                $('#exampleModalLabel').html("Добавить");
                $('#exampleModal').css('display','block');
                overlay.classList.add('active');
                typeModal = "add";
            });

            $('body').on('click','.close',function()
            {
                valClear();
                $('#exampleModal').css('display','none');
                overlay.classList.remove('active');
            });

            // Событие при добавлении картинки
            $("#image-upload").on("change",function(){
                // Обрабатываем объект добавленного изображения
                var $input = $(this),
                    reader = new FileReader();
                reader.onload = function(){
                    $("#image").attr("src", reader.result);
                };
                reader.readAsDataURL($input[0].files[0]);
            });

            // Событие при отправка AJAX запроса при клике на кнопку btn-save

            $('body').on('click', '#btn-save', function (event)
            {
                var checkbox = 0;
                var file_data       = $('#image-upload').prop('files')[0];
                var url;
                var form_data = new FormData();

                if (typeModal == "add")
                {
                    url = "admin/add"
                    if(file_data !== undefined) {
                        form_data.append('file', file_data);
                    }
                    else
                    {
                        alert("Не выбрана картинка!!!");
                        return;
                    }
                }
                if (typeModal == "update")
                {
                    url = "admin/update";
                    form_data.append('id', UpdateID);

                    if(file_data !== undefined)
                    {
                        form_data.append('file', file_data);
                    }
                }

                if ($('#checkbox').is(':checked')) { checkbox = 1; }

                form_data.append('object_name', $('#object_name').val());
                form_data.append('street', $("#street").val());
                form_data.append('house', $("#house").val());
                form_data.append('apartment', $("#apartment").val());
                form_data.append('price', $("#price").val());
                form_data.append('description', $("#description").val());
                form_data.append('checkbox', checkbox);

                $.ajax({
                    async: true,
                    type: 'POST',
                    url: url,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success:function(response) {
                        $(".table td").remove();
                        load();
                        $('#exampleModal').css('display','none');
                        overlay.classList.remove('active');
                    }
                });


                return false;
            });

        });
    </script>
<?php include(PathList::GetPath(PathList::FILE_PATH_VIEW).'footer.php'); ?>