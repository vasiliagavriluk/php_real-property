  <div class="site-footer">
		<div class="container">
			<div class="row mt-5">
				<div class="col-12 text-center">
            <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script>. All Rights Reserved; </p>
          </div>
        </div>
      </div> <!-- /.container -->
  </div> <!-- /.site-footer -->

    <!-- Preloader -->
    <div id="overlayer"></div>
    <div class="loader">
    	<div class="spinner-border" role="status">
    		<span class="visually-hidden">Loading...</span>
    	</div>
    </div>

  <script src="js/tiny-slider.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/counter.js"></script>
  <script src="js/custom.js"></script>

  </body>
  </html>