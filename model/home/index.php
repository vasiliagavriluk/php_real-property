<?php
class home {
   
    public function actionIndex() 
    {
         // Рендер на страницу Авторизации
            view::render('home','index');        
        
    }


    public function actionSearch()
    {
        $search     = $_POST['search'];

        $rows = array();

        $objPDO = new DataBase();
        $db = $objPDO->getConnection();

        if ($search == '')
        {
            $sql = 'SELECT * FROM table_RealEstate WHERE status <> 0 ORDER BY price ASC;';
        }else
        {
            $sql = 'SELECT * FROM table_RealEstate WHERE status <> 0 and house = "'.$search.'";';
        }


        $result = $db->prepare($sql);
        $result->execute();

        while($row = $result->fetch())
        {
            $rows[] = [
                'id' => $row["ID"],
                'object_name' => $row["object_name"],
                'street' => $row["street"],
                'house' => $row["house"],
                'apartment' => $row["apartment"],
                'price' => $row["price"],
                'description' => $row["description"],
                'status' => $row["status"],
                'id_image' => $row["id_image"],
            ];
        }

        // Ответ в JSON.
        header('Content-Type: application/json');
        echo json_encode($rows, JSON_UNESCAPED_UNICODE);
        exit();
    }

    public function actionSorting()
    {
        $order     = $_POST['order'];
        $rows = array();

        $objPDO = new DataBase();
        $db = $objPDO->getConnection();

        $sql = 'SELECT * FROM table_RealEstate WHERE status <> 0 ORDER BY price '.$order.';';

        $result = $db->prepare($sql);
        $result->execute();

        while($row = $result->fetch())
        {
            $rows[] = [
                'id' => $row["ID"],
                'object_name' => $row["object_name"],
                'street' => $row["street"],
                'house' => $row["house"],
                'apartment' => $row["apartment"],
                'price' => $row["price"],
                'description' => $row["description"],
                'status' => $row["status"],
                'id_image' => $row["id_image"],
            ];
        }

        // Ответ в JSON.
        header('Content-Type: application/json');
        echo json_encode($rows, JSON_UNESCAPED_UNICODE);
        exit();
    }

    public function actionAll()
    {

        $rows = array();

        $objPDO = new DataBase();
        $db = $objPDO->getConnection();

        $sql = 'SELECT * FROM table_RealEstate WHERE status <> 0 ORDER BY price ASC';

        $result = $db->prepare($sql);
        $result->execute();

        while($row = $result->fetch())
        {
            $rows[] = [
                'id' => $row["ID"],
                'object_name' => $row["object_name"],
                'street' => $row["street"],
                'house' => $row["house"],
                'apartment' => $row["apartment"],
                'price' => $row["price"],
                'description' => $row["description"],
                'status' => $row["status"],
                'id_image' => $row["id_image"],
            ];
        }

        // Ответ в JSON.
        header('Content-Type: application/json');
        echo json_encode($rows, JSON_UNESCAPED_UNICODE);
        exit();
    }

    public function actionImage()
    {
        $path = $_SERVER['REQUEST_URI'];
        // Разбиваем URL на части
        $pathParts = explode('/', $path);
        // Здесь $id номер изображения
        $id = (int)$pathParts[3];

        $objPDO = new DataBase();
        $db = $objPDO->getConnection();
        $sql = 'SELECT imagetmp FROM image_table WHERE ID = :ID';
        $result = $db->prepare($sql);
        $result->bindParam(':ID',    $id,    PDO::PARAM_STR);
        $result->execute();
        $row = $result->fetch();
        $base = $row['imagetmp'];
        //header("Content-type: image/jpg");
        echo $base;

    }
    
}
