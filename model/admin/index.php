<?php

use http\Env\Request;

class admin
{
    public function actionIndex()
    {
        // Рендер на страницу Авторизации
        view::render('admin','index');

    }

    public function actionLogin()
    {
        // Рендер на страницу Авторизации
        view::render('admin','login');
    }

    public function actionAll_list()
    {

        $rows = array();

        $objPDO = new DataBase();
        $db = $objPDO->getConnection();

        $sql = 'SELECT * FROM table_RealEstate';

        $result = $db->prepare($sql);
        $result->execute();

        while($row = $result->fetch())
        {
            $street = $row['street']." ".$row['house']."-".$row['apartment'];
            if($row['status']==1) $status = "актуальна"; else $status = "не актуальна";
            $rows[] =
            <<<TRHTML
            <tr>
               <td>{$row['object_name']}</td>
               <td>{$street}</td>
               <td>{$row['price']}</td>
               <td>{$row['description']}</td>
               <td>{$status}</td>
               <td><div class="testimonial-nav" data-id="{$row['ID']}"><span class="prev">Edit</span></div></td>
            </tr>
TRHTML;
        }

        echo json_encode($rows);
    }

    public function actionEdit()
    {
        $rows = [];

        $id     = $_POST['id'];
        $objPDO = new DataBase();
        $db = $objPDO->getConnection();
        $sql = 'SELECT * FROM table_RealEstate WHERE ID = :ID';
        $result = $db->prepare($sql);
        $result->bindParam(':ID',    $id,    PDO::PARAM_STR);
        $result->execute();
        while($row = $result->fetch())
        {
            $rows["id"] = $row["ID"];
            $rows["object_name"] = $row["object_name"];
            $rows["street"] = $row["street"];
            $rows["house"] = $row["house"];
            $rows["apartment"] = $row["apartment"];
            $rows["price"] = $row["price"];
            $rows["description"] = $row["description"];
            $rows["status"] = $row["status"];
            $rows["id_image"] = $row["id_image"];
        }
        // Ответ в JSON.
        header('Content-Type: application/json');
        echo json_encode($rows, JSON_UNESCAPED_UNICODE);
        exit();
    }

    public function actionUpdate()
    {
        $id              = $_POST['id'];
        $object_name     = $_POST['object_name'];
        $street          = $_POST["street"];
        $house           = $_POST["house"];
        $apartment       = $_POST["apartment"];
        $price           = $_POST["price"];
        $description     = $_POST["description"];
        $status          = $_POST["checkbox"];

        $arrData = [
            'id' => $id,
            'object_name' => $object_name,
            'street' => $street,
            'house' => $house,
            'apartment' => $apartment,
            'price' => $price,
            'description' => $description,
            'status' => $status,
        ];
        $objPDO = new DataBase();
        $db = $objPDO->getConnection();
        $sql = "UPDATE table_RealEstate SET object_name=:object_name, street=:street, house=:house, 
                            apartment=:apartment, price=:price, description=:description, status=:status WHERE ID=:id";

        if ($_FILES['file']['name'])
        {
            $file_name = $_FILES['file']['name'];
            $imagetmp = fopen($_FILES['file']['tmp_name'], 'rb');

            $arrData['id_image'] = $this->_insertDBImage($imagetmp,$file_name);
            $arrData['file_name'] = $_FILES['file']['name'];

            $sql = "UPDATE table_RealEstate SET object_name=:object_name, street=:street, house=:house, 
                            apartment=:apartment, price=:price, description=:description, status=:status, 
                            id_image=:id_image, file_name=:file_name WHERE ID=:id";
        }
        $db->prepare($sql)->execute($arrData);
        die();
    }

    public function actionAdd()
    {
        $object_name     = $_POST['object_name'];
        $street          = $_POST["street"];
        $house           = $_POST["house"];
        $apartment       = $_POST["apartment"];
        $price           = $_POST["price"];
        $description     = $_POST["description"];
        $status          = $_POST["checkbox"];
        $file_name = $_FILES['file']['name'];
        $imagetmp = fopen($_FILES['file']['tmp_name'], 'rb');

        //$imagetmp = addslashes(file_get_contents($_FILES['file']['tmp_name'])); //Получаем содержимое изображения и добавляем к нему слеш

        $arrData = [
            'object_name' => $object_name,
            'street' => $street,
            'house' => $house,
            'apartment' => $apartment,
            'price' => $price,
            'description' => $description,
            'status' => $status,
            'id_image' => $this->_insertDBImage($imagetmp,$file_name),
            'file_name' => $_FILES['file']['name'],
        ];

        $objPDO = new DataBase();
        $db = $objPDO->getConnection();
        $sql = "INSERT INTO table_RealEstate (object_name, street, house, apartment, price, description, status, id_image, file_name) 
                VALUES (:object_name, :street, :house, :apartment, :price, :description, :status, :id_image, :file_name)";
        $db->prepare($sql)->execute($arrData);
        die();
    }

    public function actionImage()
    {
        $path = $_SERVER['REQUEST_URI'];
        // Разбиваем URL на части
        $pathParts = explode('/', $path);
        // Здесь $id номер изображения
        $id = (int)$pathParts[3];

        $objPDO = new DataBase();
        $db = $objPDO->getConnection();
        $sql = 'SELECT imagetmp FROM image_table WHERE ID = :ID';
        $result = $db->prepare($sql);
        $result->bindParam(':ID',    $id,    PDO::PARAM_STR);
        $result->execute();
        $row = $result->fetch();
        $base = $row['imagetmp'];
        //header("Content-type: image/jpg");
        echo $base;

    }

    public function actionAut()
    {
        $login = $_POST['login'];
        $password = $_POST['password'];

        try
        {
            $pass = $this->md5pass($password);
            $objPDO = new DataBase();
            $db = $objPDO->getConnection();

            $sql = 'SELECT * FROM table_Users WHERE Login = :login AND Password = :password';

            $result = $db->prepare($sql);
            $result->bindParam(':login',    $login,    PDO::PARAM_STR);
            $result->bindParam(':password', $pass, PDO::PARAM_STR);
            $result->execute();

            $user = $result->fetch();

            if ($user) {
                $_SESSION['user'] = $user['Name'];

                $array = ['Error'=>'false','Text'=>'admin'];
            }
            else
            {
                $array = ['Error'=>'true','Text'=>'Вы ввели неправильный логин/пароль'];
            }

            echo json_encode($array);
        }
        catch (Exception $ex)
        {
            echo($ex->getMessage());
        }
    }

    public function actionClose()
    {
        unset($_SESSION['user']);
        header ('Location: /');
    }

    private function _insertDBImage($imagetmp, $file_name)
    {
        $objPDO = new DataBase();
        $db = $objPDO->getConnection();

        $sql = "INSERT INTO image_table (file_name, imagetmp) VALUES (:file_name, :imagetmp)";

        $result = $db->prepare($sql);
        $result->bindParam(':file_name',    $file_name,    PDO::PARAM_STR);
        $result->bindParam(':imagetmp', $imagetmp, PDO::PARAM_LOB);
        $result->execute();

        $lastId = $db->lastInsertId();

        return $lastId;
    }

    function md5pass($pass)
    {
        $password = $pass;            // Сам пароль
        $hash = md5($password);            // Хешируем первоначальный пароль
        $salt = "yaWqttrgh435dafITVOkZMhTXSGqOVM6cnRL1DkJ";            // Соль
        $saltedHash = md5($hash . $salt); // Складываем старый хеш с солью и пропускаем через функцию md5()
        return $saltedHash;
    }

}